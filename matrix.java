import java.util.*;
public class matrix {
public static void main(String[] args)
{
	Scanner input = new Scanner(System.in);
	int T = input.nextInt();
	for(int t = 0; t<T; t++)
	{
		int n = input.nextInt(), res = 0;
		int[][] data = new int[n][n];
		for(int i = 0; i<n; i++)
			for(int j = 0; j<n; j++)
				data[i][j] = input.nextInt();
		for(int i =0; i<n; i++)
		{
			int[] oneDim = new int[n];
			for(int j = i; j<n; j++)
			{
				for(int k = 0; k<n; k++) oneDim[k] = (oneDim[k] + data[j][k])%2;
				res += solve(oneDim);
			}
		}
		System.out.println(res);
	}
}
// Solves 1-D case
static int solve(int[] A)
{
	int even = 1, odd = 0, sum = 0, n = A.length, res = 0;
	for(int i = 0; i<n; i++)
	{
		sum = (sum + A[i])%2;
		if(sum == 0) even++;
		else odd++;
	}
	return even * (even-1)/2 + odd * (odd-1)/2;
}
// Count pairs i<=j such that sum of elements in A[i, ..., j] is divisible by n.
static int solve2(int[] A, int n)
{
	int[] count = new int[n];
	count[0] = 1;
	int sum = 0;
	for(int i = 0; i<A.length; i++)
	{
		sum = (sum + A[i])%n;
		count[sum]++;
	}
	int res = 0;
	for(int i = 0; i<n; i++) res += count[i] * (count[i]-1) / 2;
	return res;
}
}
