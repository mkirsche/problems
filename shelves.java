import java.util.*;
public class shelves {
public static void main(String[] args)
{
	Scanner input = new Scanner(System.in);
	int T = input.nextInt();
	for(int t = 0; t<T; t++)
	{
		long L = input.nextInt(), m = input.nextInt(), n = input.nextInt();
		long bestLeft = L, bestN = 0, bestM = 0;
		if(n*n > L)
		{
			for(int i = 0; i*n <= L; i++)
			{
				// Place i sticks of length n.
				long left = L - i*n;
				long leftover = left%m, j = left/m;
				if(leftover < bestLeft || (leftover == bestLeft && i > bestN))
				{
					bestLeft = leftover;
					bestN = i;
					bestM = j;
				}
			}
			System.out.println(bestM + " " + bestN + " " + bestLeft);
		}
		else
		{
			// min[i] holds the lowest number of m's that add up to a sum that is equivalent to i mod n
			long[] min = new long[(int)n];
			Arrays.fill(min,  -1);
			min[0] = 0;
			int at = 0;
			while(true)
			{
				int next = (int) ((at + m)%n);
				if(min[next] != -1) break; // We have cycled around to a value we already found, so no point in adding m anymore.
				min[next] = min[at] + 1;
				at = next;
			}
			for(int left = 0; left < n; left++)
			{
				// left is how many spaces we fail to fill
				int totalUsed = (int)(L - left);
				// Take a bunch of m's to get a value equivalent to totalUsed mod n -> we want to take as few m's as possible.
				long countM = min[(int) (totalUsed%n)];
				if(countM == -1 || countM * m > totalUsed) continue; // Such a value either cannot be reached or is not reached until after totalUsed.
				long countN = (totalUsed - countM * m) / n;
				bestLeft = left;
				bestN = countN;
				bestM = countM;
				break; // All other possible solutions will have more leftover than this, so just break now.
			}
			System.out.println(bestM + " " + bestN + " " + bestLeft);
		}
	}
}
}
